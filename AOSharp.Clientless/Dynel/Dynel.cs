﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Common.GameData;

namespace AOSharp.Clientless
{
    public class Dynel : StatHolder
    {
        public readonly Identity Identity;
        public Transform Transform { get; private set; }

        public Dynel(Identity identity, Vector3 position, Quaternion heading) : this(identity)
        {
            InitTransform(position, heading);
        }

        public Dynel(Identity identity)
        {
            Identity = identity;
        }

        protected void InitTransform(Vector3 position, Quaternion heading)
        {
            Transform = new Transform
            {
                Position = position,
                Heading = heading,
            };
        }

        public float DistanceFrom(Vector3 pos) => Vector3.Distance(Transform.Position, pos);

        public float DistanceFrom(Dynel dynel) => DistanceFrom(dynel.Transform.Position);
    }
}
