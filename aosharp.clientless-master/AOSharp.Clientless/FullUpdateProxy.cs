﻿using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;

namespace AOSharp.Clientless
{
    public class FullUpdateProxy
    {
        private static List<SimpleItemOwner> _simpleItemOwners = new List<SimpleItemOwner>();

        internal static bool Find(Identity identity, out SimpleItem simpleItem)
        {
            simpleItem = null;

            foreach (var proxy in _simpleItemOwners.SelectMany(x=>x.Dynels))
            {
                if (proxy.Identity != identity)
                    continue;

                simpleItem = proxy;

                return true;
            }

            return false;
        }

        internal static bool GetSimpleItems(Identity ownerIdentity, out List<SimpleItem> items) => (items = _simpleItemOwners.FirstOrDefault(x => x.Owner == ownerIdentity).Dynels) != null;

        internal static void OnSIFU(SimpleItemFullUpdateMessage sifu)
        {
            Identity owner = new Identity((IdentityType)sifu.OwnerType, sifu.OwnerInstance);

            if (!Find(owner, out SimpleItemOwner proxyOwner))
            {
                proxyOwner = new SimpleItemOwner(owner);
                _simpleItemOwners.Add(proxyOwner);
            }

            SimpleItem simpleItem = new SimpleItem(sifu.Identity, sifu.Position, sifu.Rotation, sifu.Stats.ToDict());

            proxyOwner.Dynels.Add(simpleItem);
            Inventory.RegisterLastItem(simpleItem);
        }

        internal static void OnCFU(ChestFullUpdateMessage cfu)
        {
            if (!Find(cfu.Owner, out SimpleItemOwner proxyOwner))
            {
                proxyOwner = new SimpleItemOwner(cfu.Owner);
                _simpleItemOwners.Add(proxyOwner);
            }

            ChestItem chestItem = new ChestItem(cfu.Identity, cfu.Stats.ToDict());

            proxyOwner.Dynels.Add(chestItem);
            Inventory.RegisterLastItem(chestItem);
        }

        internal static void OnWIFU(WeaponItemFullUpdateMessage wifu)
        {
            if (!Find(wifu.Owner, out SimpleItemOwner proxyOwner))
            {
                proxyOwner = new SimpleItemOwner(wifu.Owner);
                _simpleItemOwners.Add(proxyOwner);
            }

            WeaponItem weaponItem = new WeaponItem(wifu.Identity, wifu.Stats.ToDict());

            Inventory.RegisterLastItem(weaponItem);
            proxyOwner.Dynels.Add(weaponItem);
        }

        internal static void Reset() => _simpleItemOwners = new List<SimpleItemOwner>();

        internal static bool Find(Identity identity, out SimpleItemOwner owner) => (owner = _simpleItemOwners.FirstOrDefault(x => x.Owner == identity)) != null;
    }

    internal class SimpleItemOwner
    {
        public Identity Owner;
        public List<SimpleItem> Dynels;

        public SimpleItemOwner(Identity owner)
        {
            Owner = owner;
            Dynels = new List<SimpleItem>();
        }
    }
}