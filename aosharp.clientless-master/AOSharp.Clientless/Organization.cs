﻿using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using Newtonsoft.Json;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Clientless
{
    public class Organization
    {
        public static EventHandler<OrgInviteResponseEventArgs> OrgInviteResponse;

        internal static void OnOrgServerMessage(OrgServerMessage orgServerMessage)
        {
            if (orgServerMessage.Identity.Instance != Client.LocalDynelId)
                return;

            switch (orgServerMessage.OrgServerMessageType)
            {
                case OrgServerMessageType.OrgInvite:
                    OrgInviteResponse?.Invoke(null, new OrgInviteResponseEventArgs(orgServerMessage.Organization.Instance));
                    break;
            }
        }

        public static void Invite(Identity identity)
        {
            SendInviteOrgMessage(identity);
        }

        public static void Invite(SimpleChar simpleChar)
        {
            SendInviteOrgMessage(simpleChar.Identity);
        }

        public static void Leave()
        {
            if (DynelManager.LocalPlayer.OrgId == 0)
                return;

            Client.Send(new OrgClientMessage
            {
                Command = OrgClientCommand.Leave,
            });
        }

        //public static void Kick()
        //{
        //    if (DynelManager.LocalPlayer.OrgId == 0)
        //        return;
        //
        //    OrgClientMessage is broken so need to fix it if we ever need Kick funciton
        //}

        private static void SendInviteOrgMessage(Identity identity)
        {
            //Eventually add rank checks

            if (DynelManager.LocalPlayer.OrgId == 0)
                return;

            Client.Send(new OrgClientMessage
            {
                Command = OrgClientCommand.Invite,
                Target = identity,
                Unknown1 = 1
            });
        }
    }

    public class OrgInviteResponseEventArgs : EventArgs
    {
        public int OrgId { get; }

        public OrgInviteResponseEventArgs(int orgId)
        {
            OrgId = orgId;
        }
    }
}