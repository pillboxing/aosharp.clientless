﻿using AOSharp.Common.GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Clientless
{
    public class Transform
    {
        public Vector3 Position;
        public Quaternion Heading;
    }
}
