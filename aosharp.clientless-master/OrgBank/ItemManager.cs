﻿using AOSharp.Clientless;
using AOSharp.Clientless.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrgBank
{
    internal static class ItemManager
    {
        public static List<Backpacker> BackPacks = new List<Backpacker>();
        public static void Init()
        {
            if (!File.Exists($"{Main.Dir}\\{DynelManager.LocalPlayer.Identity.Instance}-ItemCache.json"))
            {
                Main.State = "InitialScanning";
            }
        }
    }
}
