﻿using AOSharp.Clientless;
using AOSharp.Clientless.Chat;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace OrgBank
{
    public class Main : ClientlessPluginEntry
    {
        public static string State;
        string Depositer;
        uint DepositerID;
        public static string Dir;
        public static Identity ItemToMove;
        public override void Init(string pluginDir)
        {
            Logger.Information("OrgBank Loading. wiw");
            Dir = pluginDir;
            Client.OnUpdate += OnUpdate;
            Client.Chat.PrivateMessageReceived += (e, msg) => HandlePrivateMessage(msg);

            Client.Chat.VicinityMessageReceived += (e, msg) => HandleVicinityMessage(msg);
            DynelManager.DynelSpawned += OnDynelSpawned;

            Client.CharacterInPlay += OnInPlay;
            Inventory.BankUpdated += OnBankAdd;
            Inventory.BankOpened += OnBankOpen;
            Inventory.ContainerOpened += ContainerOpened;
        }

        private void OnBankOpen()
        {
            if (State == "BankOpenWait")
            {
                ItemManager.Init();
            }
        }

        private void ContainerOpened(Container container)
        {
            Logger.Debug("Contained opened");
            Logger.Debug(container.Handle.ToString());
            Logger.Debug(container.Identity.Type.ToString());
            if (container.Identity.Type == IdentityType.Bank)
            {
                Logger.Debug("Bank opened");
            }
        }

        private void OnBankAdd(int slot)
        {
            Logger.Debug($"Moved item to bank into slot {slot}");
            ItemManager.BackPacks.Last().Location = slot;
            State = "InitialScanning";
        }

        private void OnInPlay()
        {
            OpenBags();
            State = "BankOpenWait";
            OpenBankTrusty();
            Logger.Debug("Opening bank");
        }

        private void OnUpdate(object _, double deltaTime)
        {
            switch (State)
            {
                case "Deposit":
                    if (Trade.IsTrading && (Trade.CurrentTarget != (DynelManager.Players.First(A => A.Name == Depositer).Identity) || (DynelManager.Players.First(A => A.Name == Depositer) == null)))
                    {
                        Trade.Decline();
                    }
                    if (DynelManager.Players.First(A => A.Name == Depositer) != null && !Trade.IsTrading)
                    {
                        Trade.Open(DynelManager.Players.First(A => A.Name == Depositer).Identity);
                        return;
                    }
                    if (Trade.TargetWindowCache.Items.Count == 3)
                    {
                        Trade.Confirm();
                    }
                    if (Trade.Status == TradeStatus.Confirm)
                    {
                        Trade.Accept();
                        State = "Scanning";
                    }
                    break;
                case "DepositScanning":
                    OpenBags();
                    break;
                case "InitialScanning":

                    ScanInventory();
                    break;
                case "BankScan":
                    ScanBank();
                    break;
                case "MoveToBankWait":
                    {
                       
                    }
                    break;
                case "BankOpenWait":
                    {

                    }
                    break;


            }
            //Logger.Debug("OnUpdate");
        }
        private void HandlePrivateMessage(PrivateMessage msg)
        {
            if (!msg.Message.StartsWith("!"))
                return;
            string[] commandParts = msg.Message.Split(' ');
            switch (commandParts[0].Remove(0, 1).ToLower())
            {
                case "help":
                    Client.SendPrivateMessage(msg.SenderId, "Hello!");
                    break;
                case "deposit":
                    if (State == "Deposit")
                        return;
                    Client.SendPrivateMessage(msg.SenderId, "Deposit initiated, please come close to me and deposit up to 3 backpacks");
                    Depositer = msg.SenderName;
                    DepositerID = msg.SenderId;
                    State = "Deposit";
                    break;
                case "inventory":
                    String response = "We are have the following amount of items stored in the first backpack in bank:";
                    Client.SendPrivateMessage(msg.SenderId, response + ItemManager.BackPacks[0].items.Count);
                    break;
                case "spirits":
                    Client.SendPrivateMessage(msg.SenderId, "Querying items now");
                    string Response = "We have the following number of spirits stored: ";
                    Response += QueryItems("Spirit").Count.ToString();
                    Client.SendPrivateMessage(msg.SenderId, Response);
                    break;
            }
        }
        private void HandleVicinityMessage(VicinityMsg msg)
        {
            Logger.Debug($"{msg.SenderName}: {msg.Message}");
        }

        internal static void OpenBags()
        {
            Logger.Information($"Peeking all inventory bags.");

            foreach (Item item in Inventory.Items.Where(x => x.UniqueIdentity.Type == IdentityType.Container))
            {
                item.Use();
                item.Use();
            }
        }

        internal static void ScanInventory()
        {

            foreach (Container bp in Inventory.Containers)
            {
                if (!bp.IsOpen)
                {
                    Logger.Debug("Closed backpack found");
                    bp.Item.Use();
                    return;
                }
                if (ItemManager.BackPacks.Any(A=>A.ID== bp.Identity))
                {
                    Logger.Debug("Backpack already catalogued");
                    continue;
                }
                Backpacker B = new Backpacker();
                B.ID = bp.Identity;
                foreach (Item i in bp.Items)
                {
                    B.items.Add(new BasicItem(i.Id,i.Name,i.Ql));
                }
                ItemManager.BackPacks.Add(B);
                bp.Item.MoveToBank();
                Logger.Information($"Moving {bp.Item.Name} to bank");
                State = "MoveToBankWait";
                return;
            }
            Logger.Debug($"Inventory scanned {ItemManager.BackPacks.Count}");
            State = "BankScan";
        }

        internal static void ScanBank()
        {
            foreach (Item item in Inventory.Bank.Items)
                if (item.UniqueIdentity == IdentityType.Container)
                {
                  
                }
            State = "";
        }

        private void OnDynelSpawned(object _, Dynel dynel)
        {
            Debug.WriteLine("Spawn");
        }

        private static void OpenBankTrusty()
        {
            Identity BankIdentity = new Identity(IdentityType.Terminal, -1073610185);
            Client.Send(new GenericCmdMessage
            {
                Count = 0xFF,
                Action = GenericCmdAction.Use,
                Temp4 = 1,
                User = DynelManager.LocalPlayer.Identity,
                Target = BankIdentity,
            });
        }

        public List<BasicItem> QueryItems(string Name)
        {
            List<BasicItem> items = new List<BasicItem>();
            foreach (Backpacker B in ItemManager.BackPacks)
            {
                foreach (BasicItem i in B.items)
                {
                    if (i.name.Contains(Name))
                    {
                        items.Add(i);
                    }
                }
            }
            return items;
        }
    }
}

