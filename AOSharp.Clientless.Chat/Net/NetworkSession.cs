﻿using AOSharp.Clientless.Common;
using AOSharp.Common;
using AOSharp.Common.GameData;
using Serilog.Core;
using SmokeLounge.AOtomation.Messaging.GameData;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.Messages.SystemMessages;
using SmokeLounge.AOtomation.Messaging.Serialization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace AOSharp.Clientless.Chat.Net
{
    public class NetworkSession
    {
        private NetworkStateMachine _stateMachine;
        private TcpClientEx _tcpClient;
        private ChatMessageSerializer _serializer = new ChatMessageSerializer();

        private ChatClient _chat;
        private Logger _logger;

        private Dictionary<ChatMessageType, Action<ChatMessageBody>> _chatMsgCallbacks;

        private static ConcurrentQueue<byte[]> _inboundPacketQueue = new ConcurrentQueue<byte[]>();

        internal NetworkSession(ChatClient chat)
        {
            _chat = chat;
            InitializeStateMachine();
            RegisterChatMessageHandlers();
        }

        internal void Update()
        {
            while (_inboundPacketQueue.TryDequeue(out byte[] packet))
                ProcessCachedPacket(packet);
        }

        public void Connect()
        {
            try
            {
                //DimensionInfo dimensionInfo = _chat.Dimension == Dimension.RubiKa ? DimensionInfo.RubiKa : DimensionInfo.RubiKa2019;
                DimensionInfo dimensionInfo = DimensionInfo.RubiKa;
                IPEndPoint chatServerEndpoint = new IPEndPoint(Dns.GetHostEntry(dimensionInfo.ChatServerEndpoint.Host).AddressList[0], dimensionInfo.ChatServerEndpoint.Port);
                _stateMachine.Fire(_stateMachine.ConnectTrigger, chatServerEndpoint);
            }
            catch (WebException ex)
            {
                _logger.Error($"Failed to retrieve dimension info. {ex}");
                _stateMachine.Fire(Trigger.FailedToRetreiveDimensionInfo);
            }
        }

        private void Connect(IPEndPoint endpoint)
        {

            if (_tcpClient != null && _tcpClient.Connected)
                _tcpClient.Close();

            _tcpClient = new TcpClientEx();
            _tcpClient.PacketRecv += (e, p) => _inboundPacketQueue.Enqueue(p);
            _tcpClient.Disconnected += (e, p) => _stateMachine.Fire(Trigger.Disconnect);
            _tcpClient.BeginConnect(endpoint.Address, endpoint.Port, ConnectCallback, endpoint);
        }

        private void ConnectCallback(IAsyncResult result)
        {
            try
            {
                _tcpClient.EndConnect(result);
                _stateMachine.Fire(Trigger.OnTcpConnected);
            }
            catch (Exception exception)
            {
                IPEndPoint endpoint = result.AsyncState as IPEndPoint;

                _logger.Debug($"Failed to connect to {endpoint}");

                _stateMachine.Fire(Trigger.OnTcpConnectError);
            }
        }

        public void Disconnect()
        {
            _stateMachine.Fire(Trigger.Disconnect);
        }

        public void Send(ChatMessageBody messageBody)
        {
            ChatMessage message = new ChatMessage
            {
                Body = messageBody,
                Header = new ChatHeader
                {
                    PacketType = messageBody.PacketType,
                }
            };

            Send(message);
        }

        public void Send(ChatMessage message)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                _serializer.Serialize(stream, message);
                _tcpClient.Send(stream.ToArray());
            }
        }

        public void Send(byte[] packet)
        {
            _tcpClient.Send(packet);
        }

        private void ProcessCachedPacket(byte[] packet)
        {
            try
            {
                //_logger.Debug(BitConverter.ToString(packet).Replace("-", ""));

                ChatMessage message = _serializer.Deserialize(packet);

                if (message == null)
                    return;

                _chat.NetworkMessageReceived?.Invoke(null, message);

                ChatMessageBody chatMsg = (ChatMessageBody)message.Body;
                if (_chatMsgCallbacks.TryGetValue(chatMsg.PacketType, out Action<ChatMessageBody> callback))
                    callback.Invoke(chatMsg);
            }
            catch(Exception e)
            {
               // _logger.Error($"Failed to deserialize packet: {packet.ToHexString()}");
               // _logger.Error(e.ToString());
            }
        }

        private void InitializeStateMachine()
        {
            _stateMachine = new NetworkStateMachine(_logger);

            _stateMachine.Configure(State.Idle)
                .Permit(Trigger.Connect, State.Connecting);

            _stateMachine.Configure(State.Disconnected)
              .OnEntry(() => Reconnect())
              .Permit(Trigger.Connect, State.Connecting)
              .PermitReentry(Trigger.FailedToRetreiveDimensionInfo);

            _stateMachine.Configure(State.Connecting)
                .OnEntryFrom(_stateMachine.ConnectTrigger, (endpoint) => Connect(endpoint))
                .OnEntryFrom(_stateMachine.ConnectErrorTrigger, (endpoint, exception) => Connect(endpoint))
                .Permit(Trigger.OnTcpConnectError, State.Disconnected)
                .Permit(Trigger.Disconnect, State.Disconnected)
                .Permit(Trigger.OnTcpConnected, State.Connected);

            _stateMachine.Configure(State.Connected)
                .OnEntryFrom(Trigger.OnTcpConnected, () =>
                {
                    _tcpClient.BeginReceiving();
                    _stateMachine.Fire(Trigger.ConnectionEstablished);
                })
                .PermitIf(Trigger.OnTcpConnectionError, State.Connecting, () => _chat.Config.AutoReconnect)
                .PermitIf(Trigger.OnTcpConnectionError, State.Disconnected, () => !_chat.Config.AutoReconnect)
                .Permit(Trigger.ConnectionEstablished, State.Authenticating)
                .Permit(Trigger.Connect, State.Connecting)
                .Permit(Trigger.Disconnect, State.Disconnected);

            _stateMachine.Configure(State.Authenticating)
                .SubstateOf(State.Connected)
                .Permit(Trigger.Disconnect, State.Disconnected)
                .Permit(Trigger.LoginOK, State.Chatting);

            _stateMachine.Configure(State.Chatting)
                .SubstateOf(State.Connected)
                .Permit(Trigger.Disconnect, State.Disconnected);
        }

        private void Reconnect()
        {
            _tcpClient.Close();

            if (_chat.Config.AutoReconnect)
                Task.Delay(_chat.Config.ReconnectDelay).ContinueWith(t => Connect());
            else
                _stateMachine.Fire(Trigger.Stop);
        }

        private void RegisterChatMessageHandlers()
        {
            _chatMsgCallbacks = new Dictionary<ChatMessageType, Action<ChatMessageBody>>();

            _chatMsgCallbacks.Add(ChatMessageType.ServerSalt, (msg) =>
            {
                Send(new ChatLoginRequestMessage
                {
                    Username = _chat.Credentials.Username,
                    Credentials = LoginEncryption.MakeChallengeResponse(_chat.Credentials, ((ChatServerSaltMessage)msg).ServerSalt)
                });
            });

            _chatMsgCallbacks.Add(ChatMessageType.CharacterList, (msg) =>
            {
                ChatCharacterListMessage charListMsg = (ChatCharacterListMessage)msg;

                _chat.ChatCharacterListMessageReceived?.Invoke(_chat, charListMsg);
            });

            _chatMsgCallbacks.Add(ChatMessageType.LoginOK, (msg) =>
            {
                _stateMachine.Fire(Trigger.LoginOK);
            });

            _chatMsgCallbacks.Add(ChatMessageType.LoginError, (msg) =>
            {
                ChatLoginErrorMessage loginErrorMsg = (ChatLoginErrorMessage)msg;
                _chat.ChatLoginErrorMessageReceived?.Invoke(_chat, loginErrorMsg);
                //_logger.Debug($"Failed to login.");
                //_logger.Debug($"Failed to login: {loginErrorMsg.Error}");
            });

            _chatMsgCallbacks.Add(ChatMessageType.CharacterName, (msg) =>
            {
                CharacterNameMessage charNameMsg = (CharacterNameMessage)msg;

                _chat.NameToIdMap[charNameMsg.Id] = charNameMsg.Name;
            });

            _chatMsgCallbacks.Add(ChatMessageType.PrivateMessage, (msg) =>
            {
                PrivateMsgMessage privateMsg = (PrivateMsgMessage)msg;


            });

            _chatMsgCallbacks.Add(ChatMessageType.PrivateGroupMessage, (msg) =>
            {
                PrivateGroupMessage privateMsg = (PrivateGroupMessage)msg;


            });

            _chatMsgCallbacks.Add(ChatMessageType.PrivateGroupInvite, (msg) =>
            {
                PrivateGroupInviteMessage privateGroupInviteMsg = (PrivateGroupInviteMessage)msg;


            });


            _chatMsgCallbacks.Add(ChatMessageType.VicinityMessage, (msg) =>
            {
                VicinityMessage privateMsg = (VicinityMessage)msg;


            });

            _chatMsgCallbacks.Add(ChatMessageType.ChannelList, (msg) =>
            {
                ChannelListMessage channelMsg = (ChannelListMessage)msg;
                _chat.ChannelToIdMap[channelMsg.ChannelId] = channelMsg.ChannelName;
            });

            _chatMsgCallbacks.Add(ChatMessageType.GroupMessage, (msg) =>
            {
                GroupMsgMessage groupMsg = (GroupMsgMessage)msg;

                
            });
        }
    }
}
