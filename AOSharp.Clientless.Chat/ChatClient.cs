﻿using AOSharp.Clientless.Chat.Net;
using AOSharp.Clientless.Common;
using AOSharp.Common.GameData;
using Serilog.Core;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AOSharp.Clientless.Chat
{
    public class Character
    {
        public uint Id { get; set; }
        public string Name { get; set; }

        Character(uint id, string name)
        {
            Id = id;
            Name = name;
        }


    }
    public class ChatConfig
    {
        public bool AutoReconnect = true;
        public int ReconnectDelay = 5000; //TODO: implement exponential backoff
        public int PingInterval = 60;
    }

    public class ChatClient
    {
        public readonly ChatConfig Config;
        public readonly string Character;
        public readonly Dimension Dimension;

        private readonly Logger _logger;
        private NetworkSession _netSession;
        private UpdateLoop _updateLoop;
        private double _timeTillNextPing;

        internal uint CharId;
        internal Dictionary<uint, string> NameToIdMap = new Dictionary<uint, string>();
        internal Dictionary<int, string> ChannelToIdMap = new Dictionary<int, string>();

        public EventHandler<ChatMessage> NetworkMessageReceived;
        public EventHandler<ChatCharacterListMessage> ChatCharacterListMessageReceived;
        public EventHandler<ChatLoginErrorMessage> ChatLoginErrorMessageReceived;

        internal readonly Credentials Credentials;

        public ChatClient(Credentials credentials)
        {
            Credentials = credentials;
            Config = new ChatConfig();
            _timeTillNextPing = Config.PingInterval;
        }


        public int GetChannelIdByIndex(int index) => ChannelToIdMap.ElementAt(index).Key;

        public void Init(bool useBuiltInLooper = true)
        {
            _netSession = new NetworkSession(this);

            if (useBuiltInLooper)
            {
                _updateLoop = new UpdateLoop(Update);
                _updateLoop.Start();
            }

            _netSession.Connect();
        }

        public void Update(double deltaTime)
        {
            _netSession?.Update();

            _timeTillNextPing -= deltaTime;

            if (_timeTillNextPing <= 0)
            {
                Send(new ChatPingMessage());
                _timeTillNextPing = Config.PingInterval;
            }
        }

        public void Send(ChatMessageBody msgBody)
        {
            _netSession?.Send(msgBody);
        }

        public void RemoveChannelId(int channelId)
        {
            if (ChannelToIdMap.ContainsKey(channelId))
                ChannelToIdMap.Remove(channelId); 
        }

        public bool TryGetChannelId(ChannelType channelType, out int channelId)
        {
            Type enumType = channelType.GetType();
            FieldInfo field = enumType.GetField(channelType.ToString());
            var attribute = (DescriptionAttribute)Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute));

            channelId = 0;

            foreach (var channel in ChannelToIdMap)
            {
                if (channel.Value != attribute.Description)
                    continue;

                channelId = channel.Key;
                break;
            }

            return channelId != 0;
        }
    }
    public class PrivateGroupInviteArgs : EventArgs
    {
        public uint Requester { get; }

        public PrivateGroupInviteArgs(uint requester)
        {
            Requester = requester;
        }

        //public void Accept()
        //{
        //}

        //public void Decline()
        //{
        //}
    }

    public class PrivateMessage
    {
        public uint SenderId;
        public string SenderName;
        public string Message;
    }

    public class PrivateGroupMsg
    {
        public uint ChannelId;
        public string ChannelName;
        public uint SenderId;
        public string SenderName;
        public string Message;
    }

    public class VicinityMsg
    {
        public uint SenderId;
        public string SenderName;
        public string Message;
    }

    public class GroupMsg
    {
        public uint SenderId;
        public string SenderName;
        public int ChannelId;
        public string ChannelName;
        public string Message;
    }

    public enum ChannelType
    {
        [Description("OT shopping 11-50")]
        OTShopping1150,
        [Description("OT shopping 50-100")]
        OTShopping50100,
        [Description("OT shopping 100+")]
        OTShopping100Plus,
        [Description("OT shopping11-50")]
        ClanShopping1150,
        [Description("Clan shopping 50-100")]
        ClanShopping50100,
        [Description("Clan shopping 100+")]
        ClanShopping100Plus,
        [Description("OT OOC")]
        OTOOC,
        [Description("OT German OOC")]
        OTOOCGerman,
        [Description("Clan OOC")]
        ClanOOC,
        [Description("Clan German OOC")]
        ClanOOCGerman,
    }
}
