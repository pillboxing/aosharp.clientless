﻿using AOSharp.Clientless;
using AOSharp.Clientless.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text.Json;
using System.Threading.Tasks;

namespace OrgBank
{
    internal static class ItemManager
    {
        public static List<Backpacker> BackPacks = new List<Backpacker>();
        static List<Spirit> SpiritsDB = new List<Spirit>();
        public static List<StoredSpirit> InventorySpirits = new List<StoredSpirit>();
        static List<Symbiant> SymbiantDB = new List<Symbiant>();
        public class Spirit
        {
            public Spirit(int ID, string NAME, int QL, string SLOT) { id = ID; name = NAME; ql = QL; slot = SLOT; }
            public int id { get; set; }
            public string slot { get; set; }
            public string name { get; set; }
            public int ql { get; set; }
        }
        public static void Init()
        {
            if (File.Exists($"{Main.Dir}\\JSON\\Spirits.json"))
            {
                string jsonString = File.ReadAllText($"{Main.Dir}\\JSON\\Spirits.json");
                SpiritsDB = JsonSerializer.Deserialize<List<Spirit>>(jsonString);
                Logger.Information("Spirit references loaded");
                jsonString = File.ReadAllText($"{Main.Dir}\\JSON\\Symbiants.json");
                SymbiantDB = JsonSerializer.Deserialize<List<Symbiant>>(jsonString);
                Logger.Information("Symbiant references loaded");
            }
            else
            {
                Logger.Error("Spirits.json not found, cannot start");
            }
            if (!File.Exists($"{Main.Dir}\\JSON\\{DynelManager.LocalPlayer.Identity.Instance}-ItemCache.json"))
            {
                Main.State = "init";
            }
            else
            {
                string jsonString = File.ReadAllText($"{Main.Dir}\\JSON\\{DynelManager.LocalPlayer.Identity.Instance}-ItemCache.json");
                BackPacks = JsonSerializer.Deserialize<List<Backpacker>>(jsonString);
                CreateInventoryCache();
                Main.State = "idle";
            }
        }

        static void CreateInventoryCache()
        {
            foreach (var bp in BackPacks)
            {
                foreach (var item in bp.items)
                {
                    foreach (var spirit in SpiritsDB)
                    {
                        if (spirit.id == item.id)
                        {
                            InventorySpirits.Add(new StoredSpirit(item, bp.Location,spirit.slot));
                        }
                    }
                }
            }
            Logger.Debug($"Found {InventorySpirits.Count} spirits");
        }

        public static List<BasicItem> QuerySpirits(string slot)
        {
            var ReturnList = new List<BasicItem>();
            return ReturnList;
        }
        public class Symbiant
        {
            public Symbiant(int ID, string NAME, int QL, string SLOT) { id = ID; name = NAME; ql = QL; slot = SLOT; }
            public int id { get; set; }
            public string slot { get; set; }
            public string name { get; set; }
            public int ql { get; set; }
            public int lvl { get; set; }
        }
    }
}
