﻿using AOSharp.Common.GameData;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrgBank
{
    public class BasicItem
    {
        public BasicItem(int ID, string NAME, int QL) { id = ID; name = NAME; ql = QL; }
        public int id { get; set; }
        public string name { get; set; }
        public int ql { get; set; }
    }
    public class StoredSpirit
    {
        public BasicItem item;
        public int Placement;
        public string Slot;
        public StoredSpirit(BasicItem Item, int placement, string slot) {item = Item; Placement = placement; Slot = slot; } 
    }
    public class Backpacker
    {
        public Backpacker() { items = new List<BasicItem>(); }
        public Identity ID { get; set; }
        public List<BasicItem> items { get; set; }
        public int Location { get; set;}

    }
}
