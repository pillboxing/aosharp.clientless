﻿using AOSharp.Clientless;
using AOSharp.Clientless.Chat;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace OrgBank
{
    public partial class Main : ClientlessPluginEntry
    {
        public static string State;
        string Depositer;
        uint DepositerID;
        public static string Dir;
        public static Identity ItemToMove;
        static bool BankToInventoryWait;
        static bool InventoryToBankWait;
        static bool ContainerOpenWait;
        static int InitialBankBackpackCount;
        public override void Init(string pluginDir)
        {
            Logger.Information("OrgBank: Loading.");
            Dir = pluginDir;
            Client.OnUpdate += OnUpdate;

            DynelManager.DynelSpawned += OnDynelSpawned;

            Client.CharacterInPlay += OnInPlay;
            Inventory.BankUpdated += OnBankAdd;
            Inventory.BankOpened += OnBankOpen;
            Inventory.ContainerOpened += ContainerOpened;
            Inventory.ItemAdded += InventoryItemAdd;
        }

        private void InventoryItemAdd(Item item)
        {
            BankToInventoryWait = false;
        }


        private void ContainerOpened(Container container)
        {
            if (ContainerOpenWait)
                ContainerOpenWait = false;
            if (container.Identity.Type == IdentityType.Bank)
            {
                Logger.Debug("Bank opened");
            }
        }


        private void OnInPlay()
        {
            OpenBags();
            State = "BankOpenWait";
            OpenBankTrusty();
            Logger.Debug("Opening bank");
        }

        private void OnUpdate(object _, double deltaTime)
        {
            switch (State)
            {
                case "init":
                    {
                        MoveAllBackpacksToBank();
                        break;
                    }
                case "Deposit":
                    if (Trade.IsTrading && (Trade.CurrentTarget != (DynelManager.Players.First(A => A.Name == Depositer).Identity) || (DynelManager.Players.First(A => A.Name == Depositer) == null)))
                    {
                        Trade.Decline();
                    }
                    if (DynelManager.Players.First(A => A.Name == Depositer) != null && !Trade.IsTrading)
                    {
                        Trade.Open(DynelManager.Players.First(A => A.Name == Depositer).Identity);
                        return;
                    }
                    if (Trade.TargetWindowCache.Items.Count == 3)
                    {
                        Trade.Confirm();
                    }
                    if (Trade.Status == TradeStatus.Confirm)
                    {
                        Trade.Accept();
                        State = "Scanning";
                    }
                    break;
                case "DepositScanning":
                    OpenBags();
                    break;
                case "InitialScanning":
                    ScanBank();
                    break;
                case "BankScan":

                    break;


            }
            //Logger.Debug("OnUpdate");
        }

        private void MoveAllBackpacksToBank()
        {
            if (Inventory.Containers.Count > 0)
            {
                if (InventoryToBankWait)
                 {
                    return;
                }

                if (Inventory.Bank.NumFreeSlots < Inventory.Containers.Count)
                {
                    Logger.Error("Not enough room in bank to deposit items");
                    return;
                }
                Inventory.Containers.First().Item.MoveToBank();
                InventoryToBankWait = true;
            }
            else
            {
                State = "InitialScanning";
                InventoryToBankWait = false;
            }
            Logger.Debug("All backpacks moved to bank");
            InitialBankBackpackCount = Inventory.Bank.Items.ToList().FindAll(A => A.UniqueIdentity == IdentityType.Container).Count();
            Logger.Debug($"Found {InitialBankBackpackCount} backpacks in bank");
        }

        private void HandlePrivateMessage(PrivateMessage msg)
        {
            if (!msg.Message.StartsWith("!"))
                return;
            string[] commandParts = msg.Message.Split(' ');
            switch (commandParts[0].Remove(0, 1).ToLower())
            {
                case "help":
                    Client.SendPrivateMessage(msg.SenderId, "Hello!");
                    break;
                case "deposit":
                    if (State == "Deposit")
                        return;
                    Client.SendPrivateMessage(msg.SenderId, "Deposit initiated, please come close to me and deposit up to 3 backpacks");
                    Depositer = msg.SenderName;
                    DepositerID = msg.SenderId;
                    State = "Deposit";
                    break;
                case "inventory":
                    String response = "We are have the following amount of items stored in the first backpack in bank:";
                    Client.SendPrivateMessage(msg.SenderId, response + ItemManager.BackPacks[0].items.Count);
                    break;
                case "symbiants":
                    {
                        if (commandParts.Length == 1)
                        {
                            string Window = "";
                            List<String> Slots = new List<String>(); Slots.Add("Foot"); Slots.Add("LHand"); Slots.Add("Leg"); Slots.Add("RHand"); Slots.Add("LWrist"); Slots.Add("RWrist"); Slots.Add("LArm"); Slots.Add("Chest"); Slots.Add("Waist"); Slots.Add("RArm"); Slots.Add("Ear"); Slots.Add("Head"); Slots.Add("Eye");
                            foreach (string slot in Slots)
                            {
                                Window += $"<a href='chatcmd:///tell {DynelManager.LocalPlayer.Name} !spirits {slot}'>{slot}</a>\n";
                            }
                            string Response = $"<a href=\"text://{Window}\">Spirit List</a>";
                            Client.SendPrivateMessage(msg.SenderId, Response);

                        }
                        break;
                    }
                case "spirits":
                    if (commandParts.Length == 1)
                    {
                        string Window = "";
                        List<String> Slots = new List<String>(); Slots.Add("Foot"); Slots.Add("LHand"); Slots.Add("Leg"); Slots.Add("RHand");Slots.Add("LWrist");Slots.Add("RWrist");Slots.Add("LArm");Slots.Add("Chest");Slots.Add("Waist");Slots.Add("RArm");Slots.Add("Ear");Slots.Add("Head");Slots.Add("Eye"); 
                        foreach (string slot in Slots)
                        {
                            Window += $"<a href='chatcmd:///tell {DynelManager.LocalPlayer.Name} !spirits {slot}'>{slot}</a>\n";
                        }    
                        string Response = $"<a href=\"text://{Window}\">Spirit List</a>";
                        Client.SendPrivateMessage(msg.SenderId, Response);
                        
                    }
                    if (commandParts.Length == 2)
                    {
                        switch (commandParts[1])
                        {
                            case "Head":
                                {
                                    SendSpiritList(msg.SenderId, "Head");
                                    break;
                                }
                            case "Foot":
                                {
                                    SendSpiritList(msg.SenderId, "Foot");
                                    break;
                                }
                            case "LHand":
                                {
                                    SendSpiritList(msg.SenderId, "LHand");
                                    break;
                                }
                            case "Leg":
                                {
                                    SendSpiritList(msg.SenderId, "Leg");
                                    break;
                                }
                            case "LWrist":
                                {
                                    SendSpiritList(msg.SenderId, "LWrist");
                                    break;
                                }
                            case "RWrist":
                                {
                                    SendSpiritList(msg.SenderId, "RWrist");
                                    break;
                                }
                            case "Chest":
                                {
                                    SendSpiritList(msg.SenderId, "Chest");
                                    break;
                                }
                            case "Waist":
                                {
                                    SendSpiritList(msg.SenderId, "Waist");
                                    break;
                                }
                            case "RArm":
                                {
                                    SendSpiritList(msg.SenderId, "RArm");
                                    break;
                                }
                            case "Ear":
                                {
                                    SendSpiritList(msg.SenderId, "Ear");
                                    break;
                                }
                            case "Eye":
                                {
                                    SendSpiritList(msg.SenderId, "Eye");
                                    break;
                                }
                            case "LArm":
                                {
                                    SendSpiritList(msg.SenderId, "LArm");
                                    break;
                                }
                        }
                    }
                    break;
            }
        }
        private void HandleVicinityMessage(VicinityMsg msg)
        {
            Logger.Debug($"{msg.SenderName}: {msg.Message}");
        }

        internal static void OpenBags()
        {
            Logger.Information($"Peeking all inventory bags.");

            foreach (Item item in Inventory.Items.Where(x => x.UniqueIdentity.Type == IdentityType.Container))
            {
                item.Use();
                item.Use();
            }
        }

        internal static void ScanInventory()
        {
            /*
            foreach (Container bp in Inventory.Containers)
            {
                if (!bp.IsOpen)
                {
                    Logger.Debug("Closed backpack found");
                    bp.Item.Use();
                    return;
                }
                if (ItemManager.BackPacks.Any(A=>A.ID== bp.Identity))
                {
                    Logger.Debug("Backpack already catalogued");
                    continue;
                }
                Backpacker B = new Backpacker();
                B.ID = bp.Identity;
                foreach (Item i in bp.Items)
                {
                    B.items.Add(new BasicItem(i.Id,i.Name,i.Ql));
                }
                ItemManager.BackPacks.Add(B);
                bp.Item.MoveToBank();
                Logger.Information($"Moving {bp.Item.Name} to bank");
                State = "MoveToBankWait";
                return;
            }
            Logger.Debug($"Inventory scanned {ItemManager.BackPacks.Count}");
            State = "BankScan";*/
        }

        internal static void ScanBank()
        {
            if (Inventory.Containers.Count == 0 && ItemManager.BackPacks.Count == InitialBankBackpackCount)
            {
                Logger.Information("OrgBank: Scanning of bank complete");
                String JsonString = JsonSerializer.Serialize(ItemManager.BackPacks);
                using (StreamWriter outputFile = new StreamWriter(Path.Combine($"{Main.Dir}\\JSON\\", $"{DynelManager.LocalPlayer.Identity.Instance}-ItemCache.json")))
                {
                    outputFile.Write(JsonString);
                }
                State = "Idle";
                return;
            }
            if (Inventory.Containers.Count == 1)
            {
                if (ContainerOpenWait || InventoryToBankWait)
                {
                    return;
                }

                if (!Inventory.Containers[0].IsOpen)
                {
                    Inventory.Containers[0].Item.Use();
                    ContainerOpenWait = true;
                    Logger.Debug("Waiting for container to open");
                    return;
                }
                else
                {
                    Logger.Debug($"Scanning {Inventory.Containers[0].Item.Name} - Count: {Inventory.Containers[0].Items.Count}");
                    Backpacker B = new Backpacker();
                    B.ID = Inventory.Containers[0].Identity;
                    foreach (Item i in Inventory.Containers[0].Items)
                    {
                        B.items.Add(new BasicItem(i.Id, i.Name, i.Ql));
                    }
                    ItemManager.BackPacks.Add(B);
                    Logger.Debug($"{ItemManager.BackPacks.Count}");
                    Inventory.Containers[0].Item.MoveToBank();
                    InventoryToBankWait = true;
                    Logger.Information($"Moving {Inventory.Containers[0].Item.Name} to bank");
                    return;
                }

            }
            if (BankToInventoryWait)
            {
                return;
            }
            foreach (Item item in Inventory.Bank.Items)
                if (item.UniqueIdentity == IdentityType.Container)
                {
                    if (ItemManager.BackPacks.Find(A => A.Location == item.Slot.Instance) != null)
                    {
                        Logger.Debug($"Backback already found at {(Inventory.Bank.Items.ToList().IndexOf(item) + 1)}");
                        continue;
                    }

                    item.MoveToInventory();
                    Logger.Information($"Moving {item.Name} to inventory.");
                    BankToInventoryWait = true;
                    return;
                }

        }

        private void OnDynelSpawned(object _, Dynel dynel)
        {
        }

        private static void OpenBankTrusty()
        {
            Identity BankIdentity = new Identity(IdentityType.Terminal, -1073610185);
            Client.Send(new GenericCmdMessage
            {
                Count = 0xFF,
                Action = GenericCmdAction.Use,
                Temp4 = 1,
                User = DynelManager.LocalPlayer.Identity,
                Target = BankIdentity,
            });
        }

        public List<BasicItem> QueryItems(string Name)
        {
            List<BasicItem> items = new List<BasicItem>();
            foreach (Backpacker B in ItemManager.BackPacks)
            {
                foreach (BasicItem i in B.items)
                {
                    if (i.name.Contains(Name))
                    {
                        items.Add(i);
                    }
                }
            }
            return items;
        }
    }
}

