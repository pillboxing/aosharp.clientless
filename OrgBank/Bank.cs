﻿using AOSharp.Clientless;
using AOSharp.Clientless.Logging;
using AOSharp.Common.GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrgBank
{
    public partial class Main : ClientlessPluginEntry 
    {
        private void OnBankOpen()
        {
            if (State == "BankOpenWait")
            {
 
                ItemManager.Init();

            }
        }
        private void OnBankAdd(int slot)
        {
            InventoryToBankWait = false;
            Logger.Debug($"Moved item to bank into slot {slot}");
            if (State == "init")
            {
                return;
            }
            Logger.Debug($"Updated backpack list - {slot}");
            ItemManager.BackPacks.Last().Location = slot;

        }
    }

}
