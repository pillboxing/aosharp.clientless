﻿using AOSharp.Clientless;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OrgBank.ItemManager;

namespace OrgBank
{
    public partial class Main : ClientlessPluginEntry
    {

        void SendSpiritList(uint characterid, string slot)
        {
            List<String> ProcessedSpirits = new List<String>();
            string Window = "";
            var Spirits = ItemManager.InventorySpirits.FindAll(A => A.Slot == slot);

            Spirits.OrderBy(A => A.item.ql);
            foreach (var spirit in Spirits)
            {
                if (ProcessedSpirits.Contains(spirit.item.name))
                    continue;
                    var result = Spirits.FindAll(A => A.item.name == spirit.item.name);
                    int count = result.Count();

                    if (count > 1)
                    {
                        Window += $"<a href='itemref://{spirit.item.id}/{spirit.item.id}/{spirit.item.ql}'>{spirit.item.name}</a>x{count}\n";
                    }
                    else
                    {
                        Window += $"<a href='itemref://{spirit.item.id}/{spirit.item.id}/{spirit.item.ql}'>{spirit.item.name}</a>\n";
                    }
                    ProcessedSpirits.Add(spirit.item.name);
            }
            string Response = $"<a href=\"text://{Window}\">{slot} Spirits</a>";
            Client.SendPrivateMessage(characterid, Response);
        }
    }
}
